var usersFile = require('./login_users.json');
app.get('/apitechu/v1', function(request, response){
    response.send({"bienvenida":"Bienvenido", "Hoy es":new Date()});
})


app.get('/apitechu/v1/usuarios', function(req, res){
  res.send(usersFile);
})

app.post('/apitechu/v1/usuarios', function(req, res){
  var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country, "last_name":req.headers.last_name
, "id":req.headers.id};
  usersFile.push(nuevo);
  console.log(req.headers);
  const datos = JSON.stringify(usersFile);
  fs.writeFile("./usuarios.json", datos, function(err){
    if (err)
      console.log(err);
    console.log("Fichero guardado");
  });

  res.send("Alta OK");
})

app.post('/apitechu/v2/usuarios', function(req, res){

  var nuevo = req.body;
  usersFile.push(nuevo);
  console.log(req.body);
  const datos = JSON.stringify(usersFile);
  fs.writeFile("./usuarios.json", datos, function(err){
    if (err)
      console.log(err);
    console.log("Fichero guardado");
  });

  res.send("Alta OK v2");
})



app.delete('/apitechu/v1/usuarios/:id', function(req, res){
  usersFile.splice(req.params.id-1, 1);
  res.send("Usuario borrado");
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res){
  console.log("parametros");
  console.log(req.params);
  console.log("Query-String");
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers);
  console.log("Body");
  console.log(req.body)
  res.send("respuesta")
})

app.post('/apitechu/v1/login', function(req, res){
  var attemptingUser = {"email":req.headers.email, "password":req.headers.password};
  var userExists = 'N';
  var i = 0;
  while (i < usersFile.length && userExists == 'N') {
    var currentUser = usersFile[i];
    if (currentUser.email == attemptingUser.email && currentUser.password == attemptingUser.password){
      //el usuario existe y la contraseña coincide
      currentUser.isLogged = 'Y';
      userExists = 'Y';
      usersFile[i] = currentUser;
      attemptingUser = currentUser;
    }
    i++;
  }

  if(userExists == 'N'){
    res.send({"encontrado":"NO"});
  }else if (userExists == 'Y') {
    //Usuario logado correctamente
    //actualizamos el fichero de usuarios
    const datos = JSON.stringify(usersFile);
    fs.writeFile("./login_users.json", datos, function(err){
      if (err)
        console.log(err);
      console.log("Fichero de login guardado");
    });
    console.log('USUARIO: '+attemptingUser.email+' LOGADO');
    res.send({"encontrado":"SI", "id":attemptingUser.id});
  }
})

app.post('/apitechu/v1/logout', function(req, res){
  var attemptingUser = {"id":req.headers.id};
  console.log("intentando login de usuario "+req.headers.id);
  var userExists = 'N';
  var i = 0;
  while (i < usersFile.length && userExists == 'N') {
    var currentUser = usersFile[i];
    if (currentUser.id == attemptingUser.id && currentUser.isLogged == 'Y'){
      //el usuario existe, la contraseña coincide, y tiene sesion abierta
      currentUser.isLogged = 'N';
      userExists = 'Y';
      usersFile[i] = currentUser;
      attemptingUser = currentUser;
    }
    i++;
  }

  if(userExists == 'N'){
    res.send({"encontrado":"NO"});
  }else if (userExists == 'Y') {
    //Usuario logado correctamente
    //actualizamos el fichero de usuarios
    const datos = JSON.stringify(usersFile);
    fs.writeFile("./login_users.json", datos, function(err){
      if (err)
        console.log(err);
      console.log("Fichero de login guardado");
    });
    console.log('USUARIO: '+attemptingUser.email+' HA FINALIZADO SESION');
    res.send({"encontrado":"SI", "id":attemptingUser.id});
  }
})
